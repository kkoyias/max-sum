#include <assert.h>
#include <stdio.h>
#include "matrix.h"

/*
 * a recursive solution to the maximum sum problem
 */
uint32_t __solve_rec(matrix_t *, uint32_t, uint32_t);
uint32_t solve_rec(matrix_t *matrix){
    uint32_t max, col;
    assert(matrix);

    // for each possible starting cell return the one to get us the best result
    for(col = max = 0; col < matrix->cols; col++)
        max = MAX(max, __solve_rec(matrix, 0, col));
    return max;
}

uint32_t __solve_rec(matrix_t *matrix, uint32_t row, uint32_t col){
    uint32_t left_cell, left, right, right_cell, middle_cell, max;
    assert(matrix && row < matrix->rows && col < matrix->cols);

    // base case: final row of the matrix
    if(row == matrix->rows - 1)
        return *matrix_get(matrix, row, col);
    
    // get left and right column indexes of the row right under
    left = col > 0 ? col - 1 : matrix->cols - 1; 
    right = col < matrix->cols - 1 ? col + 1 : 0;

    // get the corresponding values, for left-middle-right
    left_cell = __solve_rec(matrix, row + 1, left);
    right_cell = __solve_rec(matrix, row + 1, right);
    middle_cell = __solve_rec(matrix, row + 1, col);

    // get maximum of left-right-middle
    max = MAX(MAX(left_cell, right_cell), middle_cell);

    // return max plus current cell value
    return max + *matrix_get(matrix, row, col);
}