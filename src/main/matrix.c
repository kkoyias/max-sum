#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "matrix.h"
#include "time.h"

int matrix_init(matrix_t *matrix, uint32_t rows, uint32_t cols){
    assert(matrix);

    // allocate space for $rows pointers and $rows * $cols total cells and fill with 1s
    if((matrix->data = calloc(rows * cols, sizeof(uint32_t))) == NULL)
        return -1;
    memset(matrix->data, UINT32_MAX, sizeof(uint32_t) * rows * cols);

    matrix->rows = rows;
    matrix->cols = cols;
    return 0;
}

// set value on cell [i][j] of the matrix
int matrix_set(matrix_t *matrix, uint32_t value, uint32_t row, uint32_t col){
    assert(matrix && row < matrix->rows && col < matrix->cols);
    
    matrix->data[row * matrix->cols + col] = value;
    return 0;
}

// get value from cell [i][j] of the matrix
uint32_t* matrix_get(matrix_t *matrix, uint32_t row, uint32_t col){
    assert(matrix && row < matrix->rows && col < matrix->cols);
    
    return matrix->data + row * matrix->cols + col;
}

// release resources allocated for the matrix
void matrix_free(matrix_t *matrix){
    assert(matrix);
    matrix->rows = matrix->cols = 0;

    free(matrix->data);
    matrix->data = NULL;
}

// display a matrix
void matrix_print(matrix_t *matrix){
    uint32_t i, j, value;
    assert(matrix);

    // display meta-data
    fprintf(stdout, 
            "\e[1;4mMatrix\e[0m\n * rows: %u\n * cols: %u\n * cells\n", 
            matrix->rows, matrix->cols);

    // display cell values, use -1 for empty cells
    for(i = 0; i < matrix->rows; i++){
        for(j = 0; j < matrix->cols; j++){
            value = *matrix_get(matrix, i, j);
            fprintf(stdout, value < UINT32_MAX ? "%-10u" : "%-10d", value);
        }
        fputc('\n', stdout);
    }
}

void matrix_randomize(matrix_t *matrix){
    assert(matrix);

    srand(time(NULL));
    for(uint32_t i = 0; i < matrix->rows; i++)
        for(uint32_t j = 0; j < matrix->cols; j++)
            *matrix_get(matrix, i, j) = rand() % UINT8_MAX;
}

uint32_t matrix_row_max(matrix_t *matrix, uint32_t row){
    uint32_t max, col;
    assert(matrix && row < matrix->rows);

    for(max = col = 0; col < matrix->cols; col++)
        max = MAX(max, *matrix_get(matrix, row, col));

    return max;
}