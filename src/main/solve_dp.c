#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "matrix.h"

/*
 *  an iterative bottom-up approach on the max sum problem, using dynamic programming
 */ 
void __solve_dp(matrix_t *, matrix_t *);
void __path_get(matrix_t *, uint32_t *, uint32_t);
void __path_print(matrix_t *, uint32_t *);

uint32_t solve_dp(matrix_t *matrix){
    uint32_t *path, max_sum, col, j, cell;
    matrix_t memo;
    assert(matrix);

    // initialize a memoization matrix with the same last-row as the original
    matrix_init(&memo, matrix->rows, matrix->cols);
    memcpy(matrix_get(&memo, memo.rows - 1, 0), matrix_get(matrix, matrix->rows - 1, 0), sizeof(uint32_t) * memo.cols);

    // estimate the rest of the memo based on the matrix
    __solve_dp(matrix, &memo);

    // get starting cell of the optimal solution
    // as well as the index to reconstruct the path
    for(max_sum = col = j = 0; j < memo.cols; j++){
        if((cell = *matrix_get(&memo, 0, j)) > max_sum){
            col = j;
            max_sum = cell;
        }
    }

    // get & display the path if requested
    #ifdef PATH
        path = calloc(matrix->rows, sizeof(uint32_t));
        __path_get(matrix, path, col);
        __path_print(matrix, path);
        free(path);
    #endif

    // release resources
    matrix_free(&memo);
    return max_sum; 
}

void __solve_dp(matrix_t *matrix, matrix_t *memo){
    uint32_t left, left_cell, right, right_cell, middle_cell, max;
    assert(matrix && memo);

    for(uint32_t i = memo->rows - 2; i != UINT32_MAX; i--){
        for(uint32_t j = 0; j < memo->cols; j++){

            // get left and right column indexes of next row
            left = j > 0 ? j - 1 : memo->cols - 1; 
            right = j < memo->cols - 1 ? j + 1 : 0;

            // get the corresponding values
            left_cell = *matrix_get(memo, i + 1, left);
            right_cell = *matrix_get(memo, i + 1, right);
            middle_cell = *matrix_get(memo, i + 1, j);

            // get maximum of left-right-middle
            max = MAX(MAX(left_cell, right_cell), middle_cell);
            matrix_set(memo, max + *matrix_get(matrix, i, j), i, j);
        }
    }
}

void __path_get(matrix_t *matrix, uint32_t *path, uint32_t col){
    uint32_t left, right, middle, left_cell, right_cell, middle_cell;
    for(uint32_t i = 0; i < matrix->rows - 1; i++){
        path[i] = col;

        // get left, middle and right column of the next row
        left = col > 0 ? col -1  : matrix->cols - 1; 
        right = col < matrix->cols - 1 ? col + 1 : 0;
        middle = col < matrix->cols - 1 ? col + 1 : 0;

        // get the corresponding values
        left_cell = *matrix_get(matrix, i + 1, left);
        middle_cell = *matrix_get(matrix, i + 1, col);
        right_cell = *matrix_get(matrix, i + 1, right);

        // get index of the maximum
        if(left_cell < middle_cell)
            col = middle_cell < right_cell ? right : middle;
        else
            col = left_cell < right_cell ? right : left;
    }

    path[matrix->rows-1] = col;
}

void __path_print(matrix_t *matrix, uint32_t *path){
    assert(matrix && path);

    for(uint32_t i = 0; i < matrix->rows; i++){
        for(uint32_t j = 0; j < matrix->cols; j++)
            fprintf(stdout, j != path[i] ? "%-10u" : "\e[1;33m%-10u\e[0m", *matrix_get(matrix, i, j));
        fputc('\n', stdout);
    }
}
