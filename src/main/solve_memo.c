#include <string.h>
#include <assert.h>
#include "matrix.h"

/*
 *  another recursive solution to the max-sum problem using memoization
 */ 
uint32_t __solve_memo(matrix_t *, matrix_t *, uint32_t, uint32_t);
uint32_t __solve_memo(matrix_t*, matrix_t*, uint32_t, uint32_t);
uint32_t solve_memo(matrix_t *matrix){
    matrix_t memo;
    uint32_t max_sum;
    assert(matrix);

    // pass a matrix to the recursive call for the estimated values to be stored
    matrix_init(&memo, matrix->rows, matrix->cols);
    memcpy(matrix_get(&memo, memo.rows - 1, 0), matrix_get(matrix, matrix->rows - 1, 0), sizeof(uint32_t) * memo.cols);

    // solve for each possible starting cell
    for(uint32_t col = 0; col < matrix->cols; col++)
        max_sum = MAX(max_sum, __solve_memo(matrix, &memo, 0, col));

    // release resources and return max sum
    matrix_free(&memo);
    return max_sum;
}

uint32_t __solve_memo(matrix_t *matrix, matrix_t *memo, uint32_t row, uint32_t col){
    uint32_t left_cell, left, right, right_cell, middle_cell, max, this_cell;
    assert(matrix && memo && row < matrix->rows && col < matrix->cols);

    // base case: final row of the matrix
    if(row == matrix->rows - 1)
        return *matrix_get(matrix, row, col);
    else if(*matrix_get(memo, row, col) != UINT32_MAX)
        return *matrix_get(memo, row, col);
    
    // get indexes of left and right column for the row right under  
    left = col > 0 ? col - 1 : matrix->cols - 1; 
    right = col < matrix->cols - 1 ? col + 1 : 0;

    // get the corresponding values for left-right-middle
    left_cell = __solve_memo(matrix, memo, row + 1, left);
    right_cell = __solve_memo(matrix, memo, row + 1, right);
    middle_cell = __solve_memo(matrix, memo, row + 1, col);

    // get maximum of left-right-middle
    max = MAX(MAX(left_cell, right_cell), middle_cell);

    // return max plus current cell value and update memo
    this_cell = *matrix_get(matrix, row, col);
    matrix_set(memo, max + this_cell, row, col);
    return max + this_cell;
}