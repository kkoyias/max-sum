#ifndef _matrix_H
#define _matrix_H
    #include <stdint.h>

    #define MAX(a,b) ((a) > (b) ? (a) : (b)) 

    typedef struct matrix{
        uint32_t *data;
        uint32_t rows;
        uint32_t cols;
    }matrix_t;

    // member methods
    int matrix_init(matrix_t *, uint32_t, uint32_t);
    int matrix_set(matrix_t *, uint32_t, uint32_t, uint32_t);
    uint32_t* matrix_get(matrix_t *, uint32_t, uint32_t);
    void matrix_free(matrix_t *);
    void matrix_print(matrix_t *matrix);
    void matrix_randomize(matrix_t *matrix);
    uint32_t matrix_row_max(matrix_t *matrix, uint32_t);

    // problem-solving methods
    uint32_t solve_rec(matrix_t*);
    uint32_t solve_memo(matrix_t*);
    uint32_t solve_dp(matrix_t*);

#endif 