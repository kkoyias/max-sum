#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "matrix.h"

// define default values, they can be overriden by cmd arguments
#define ROWS 3
#define COLS 6
#define PROPS 3
#define WAYS 3
#define PRINT 0

int main(int argc, char *argv[]){
    uint32_t rows = ROWS, cols = COLS, ws = WAYS, *props[PROPS] = {&rows, &cols, &ws}, max_sum, print = PRINT;
    uint32_t (*ways[WAYS])(matrix_t *) = {solve_dp, solve_memo, solve_rec}; //methods to be tested
    char *prop_names[PROPS] = {"-r", "-c", "-w"};
    char *ways_names[WAYS] = {"\e[1;33mDynamic Programming", "\e[1;32mMemoization", "\e[1;31mRecursive"}; 
    matrix_t matrix;
    time_t start, end;

    // get properties from command line
    for(int i = 0; i < argc; i++){
        if(print == 0 && strcmp(argv[i], "--print") == 0){
            print = 1;
            continue;
        }

        for(int j = 0; j < PROPS && i < argc - 1; j++)
            if(strcmp(argv[i], prop_names[j]) == 0)
                *props[j] = atoi(argv[i + 1]);
    }

    // initialize and randomize the matrix
    matrix_init(&matrix, rows, cols);
    matrix_randomize(&matrix);

    // use each function to get the maximum sum
    for(int i = 0; i < ws; i++){
        start = clock();
        max_sum = ways[i](&matrix);
        end = clock();
        fprintf(stdout, "Maximum sum with method %s\e[0m: %u - %.2f CPU secs\n",
                        ways_names[i], max_sum, ((float)end - start)/CLOCKS_PER_SEC);
    }

    if(print)
        matrix_print(&matrix);

    // release all resources and exit
    matrix_free(&matrix);
    return 0;
}