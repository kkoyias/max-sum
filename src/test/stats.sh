#!/bin/bash

#configuration
exe=$(git rev-parse --show-toplevel)/src/test/main
cols=5
out=out.md
max_memo=10000 
max_rec=21
rows=(15 18 21 100 1000 10000 100000 1000000)
header=("N" "M" "Dynamic" "Memoization" "Recursive")

# ensure that the executable exists
if [ ! -e $exe ]
then
    echo -e "\e[1;31mError\e[0m: executable not found"
    exit 1
fi

# if markdown to be produced does not exist, create
echo "|${header[@]}|" | tr ' ' '|' > $out
echo "|${header[@]}|" | tr ' ' '|' | tr "[a-z][A-Z][0-9]()_" '-' >> $out

# execute for various matrix sizes
for r in ${rows[@]}
do
    if [ $r -gt $max_memo ]
    then
        ways=1
    elif [ $r -gt $max_rec ]
    then
        ways=2
    else
        ways=3
    fi

    res=$($exe -r $r -c $cols -w $ways | tail -3 | cut -d ':' -f 2 | cut -d '-' -f2 | cut -d ' ' -f2 | tr '\n' '|')
    echo "|$r|$cols|${res[@]}" >> $out
done

exit 0