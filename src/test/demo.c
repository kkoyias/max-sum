#include <stdio.h>
#include "matrix.h"

#define WAYS 3

int main(){
    uint32_t rows, cols;
    matrix_t matrix;
    uint32_t (*ways[WAYS])(matrix_t *) = {solve_dp, solve_memo, solve_rec}; //methods to be tested
    char *ways_names[WAYS] = {"\e[1;33mDynamic Programming", "\e[1;32mMemoization", "\e[1;31mRecursive"}; 

    // get dimensions of matrix from stdin
    fscanf(stdin, "%u%u", &rows, &cols);

    // create matrix
    matrix_init(&matrix, rows, cols);

    // fill matrix with values read from stdin
    for(uint32_t i = 0; i < rows * cols && fscanf(stdin, "%u", matrix.data + i) == 1; i++);


    // solve max-sum with a variety of different approaches
    for(int i = 0; i < WAYS; i++)
        fprintf(stdout, "max-sum with %s: \e[0;1;4m%u\e[0m\n", ways_names[i], ways[i](&matrix));

    // free all resources allocated for this matrix
    matrix_free(&matrix);
    return 0;
}