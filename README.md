# max-sum

## Intro

This project uses three different algorithms
on a simple optimization problem and compares their performance
in terms of time and space efficiency.

## The problem

**max-sum** is defined as the maximum possible sum one
can get traversing a matrix starting from any cell of the top-row
all the way down to the very last row, moving one row
at a time, using the cell right under or any of it's adjacent.  
For example, being at row i and column j of a NxM matrix,
there are always three valid options based on the following formula.

* **middle**: matrix[i + 1][j]
* **left**: if j == 0 then matrix[i + 1][M - 1] or else matrix[i + 1][j - 1]
* **right**: if j == M - 1 then matrix[i + 1][0] or else matrix[i + 1][j + 1]

## Algorithms

The following three approaches wre used:

### Recursive

This is a brute force recursive approach, ignoring overlapping amongst cases.
As expected, this was the slowest of all and time increased exponentially
relative to the matrix size.

### Memoization

This is another recursive approach using an extra matrix where all values
estimated are stored and retrieved instead of being estimated again
in case they are needed in another sub-problem, having a polynomial
runtime, being efficient even for very large matrixes.

### Dynamic Programming

This is an iterative approach using an extra matrix as well,
building all the way up from the base case,
which is the last row, knowing that the maximum sum of every cell
in that row is it's value.
This approach was even faster, due to the fact that there are no
recursive calls and therefore no overhead and no danger for the
stack to overflow.
The runtime of the dynamic approach is **O(NxM)**.

## Run it

Under `/src/test` one can find a comparison of these approaches
under `main.c` and an example for a [small input](./rsrc/input.txt) under `demo.c`
To run those sample, type as follows:

``` bash
max-sum >> cd ./src/test
max-sum/src/test >> make
max-sum/src/test >> ./main -r <rows> -c <cols> -w <ways>
max-sum/src/test >> ./demo < ../../rsrc/input.txt
```

To disable path-printing, put **line 6** of the `makefile` into comments,
then `make clean` and `make` again.
This flag should also be disabled when [stats.sh](./src/test/stats.sh) is running.
Use `-w 2` to omit the recursive solution, which will take too long for
a matrix with more than 20 rows.

## Results

After experimenting with various matrix row sizes, the following
results in CPU seconds where collected

|N|M|Dynamic Prog.|Memoization|Recursive|
|-|-|-------|-----------|---------|
|15|5|0.00|0.00|0.50|
|18|5|0.00|0.00|14.04|
|21|5|0.00|0.00|445.34|
|100|5|0.00|0.00|
|1000|5|0.00|0.00|
|10000|5|0.00|0.00|
|100000|5|0.02|
|1000000|5|0.16|

Memoization recursive approach caused a stack-overflow for a
matrix row size larger than or equal to 100.000, due to the
large number of recursive calls.

## Conclusion

All algorithms above guaranteed to find the optimal solution,
but the time complexity can decrease dramatically by re-using
the solution for a sub-problem to build one for the main problem,
instead of estimating the same thing over and over again.
